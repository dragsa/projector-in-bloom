package org.gnat.cbf.hash

import com.typesafe.config.ConfigFactory

import scala.util.hashing.{ByteswapHashing, MurmurHash3}

object HashProvider {
  type HashFunction[T] = Int => Int => T => Int
  //  type HashFunctionNormal[T] = Int => T => Int

  val normalizer: Int => Long => Int = {
    expectedBfLength => input => {
//      println(s"input: $input")
//      println(s"DF length $expectedBfLength")
//      println(s"input positive ${input.toInt & 0x7fffffff}")
      (input.toInt & 0x7fffffff) % expectedBfLength
    }
  }

  val hfConfig = ConfigFactory.load().getConfig("hash-function")

  def getHashFunction[T](name: String = hfConfig.getString("defaults.type")): HashFunction[T] = {
    name match {
      case HashFunctionTypes.Murmur3 => murmur3[T]
      case HashFunctionTypes.Byteswap => byteswap[T]
      case HashFunctionTypes.Cityhash => cityhash[T]
      case _ => throw new RuntimeException("non-existing hash function")
    }
  }

  def murmur3[T]: HashFunction[T] = (seed: Int) => (modulo: Int) => (hashedValue: T) =>
    normalizer(modulo)(MurmurHash3.stringHash(hashedValue.toString, seed))

  def byteswap[T]: HashFunction[T] = (_: Int) => (modulo: Int) => (hashedValue: T) =>
    normalizer(modulo)(new ByteswapHashing[T].hash(hashedValue))

  def cityhash[T]: HashFunction[T] = (_: Int) => (modulo: Int) => (hashedValue: T) => {
    // Cityhash is for Strings only
    val hashedValueAsString = hashedValue.toString
    normalizer(modulo)(CityHash.cityHash64(hashedValueAsString.getBytes, 0, hashedValueAsString.length))
  }
}

object HashFunctionTypes {
  val Murmur3 = "MURMUR3"
  val Byteswap = "BYTESWAP"
  val Cityhash = "CITYHASH"

  def allFunctions = Set(Murmur3, Byteswap, Cityhash)
}
