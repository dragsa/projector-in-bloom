import org.gnat.cbf.hash.{HashFunctionTypes, HashProvider}

import scala.util.Random

for (_ <- 1 to 10) {
  println(Random.alphanumeric.take((new Random).nextInt(10)).mkString)
}

Seq.fill(20)(Random.alphanumeric.take((new Random).nextInt(2)).distinct.mkString).toSet

Math.log(Math.E)


val func = HashProvider.getHashFunction[String](HashFunctionTypes.Murmur3)( 1)

- 100 * Math.log(0.1) / Math.pow(Math.log(2), 2)

val list = List(2, 3, 4 ,5)
val step = Integer.MAX_VALUE / list.length
val rnd = Random
list.zipWithIndex.map{ case (a, b) => (a, (b + 1) + rnd.nextInt(step*(b+1) - (b+1))) }
(1 to Integer.MAX_VALUE by Integer.MAX_VALUE / (3)).toList

println(-1324982000 & 0x7fffffff)

val vector = Vector(1, 2, 3, 4, 5)
val pointToUpdate = Seq(1, 3)

pointToUpdate
  .foldLeft(vector)((currentStorage, index) => currentStorage.updated(index, currentStorage(index) + 10))

(-10l / 2).toInt & 0x7fffffff
Integer.toBinaryString((-10l / 2).toInt & 0x7fffffff)