package org.gnat.cbf.filter

case class CBFImmutable[T](override val positiveFalseProbability: Double,
                           override val expectedNumberOfElements: Int,
                           storage: Vector[Int] = Vector())(
                            implicit override val hashFunctionToUse: String)
  extends CBF[T](positiveFalseProbability, expectedNumberOfElements, hashFunctionToUse) {

  val bfStorage = if (storage.isEmpty) Vector.fill[Int](bfSize)(0) else storage

  def add(element: T): CBF[T] = {
    val newBfStorage = hashFunctionApplications(element)
      .foldLeft(bfStorage)((currentStorage, index) => currentStorage.updated(index, currentStorage(index) + 1))
    CBFImmutable[T](positiveFalseProbability, expectedNumberOfElements, newBfStorage)(hashFunctionToUse)
  }

  def addAll(elements: Set[T]): CBF[T] = {
    val newBfStorage = elements.foldLeft(bfStorage)((currentStorage, element) => hashFunctionApplications(element)
      .foldLeft(currentStorage)((currentStorageInner, index) => currentStorageInner.updated(index, currentStorageInner(index) + 1)))
    CBFImmutable[T](positiveFalseProbability, expectedNumberOfElements, newBfStorage)(hashFunctionToUse)
  }

  def contains(element: T): Boolean = {
    //    val hashes = hashFunctionApplications(element)
    //
    //    val test = hashes.map(bfStorage(_)).forall(_ > 0)
    //    println(s"$hashes: $test - $element")
    //    test
    hashFunctionApplications(element).map(bfStorage(_)).forall(_ > 0)
  }

  def containsAll(elements: Set[T]): Boolean =
    elements.forall(element => hashFunctionApplications(element).map(bfStorage(_)).forall(_ > 0))

  def remove(element: T): CBF[T] = {
    val newBfStorage = hashFunctionApplications(element)
      .foldLeft(bfStorage)((currentStorage, index) => {
        val counter = currentStorage(index)
        if (counter == 0) currentStorage
        else currentStorage.updated(index, currentStorage(index) - 1)
      })
    CBFImmutable[T](positiveFalseProbability, expectedNumberOfElements, newBfStorage)(hashFunctionToUse)
  }

  def removeAll(elements: Set[T]): CBF[T] = {
    val newBfStorage = elements.foldLeft(bfStorage)((currentStorage, element) => hashFunctionApplications(element)
      .foldLeft(currentStorage)((currentStorageInner, index) => {
        val counter = currentStorageInner(index)
        if (counter == 0) currentStorageInner
        else currentStorageInner.updated(index, currentStorageInner(index) - 1)
      }))
    CBFImmutable[T](positiveFalseProbability, expectedNumberOfElements, newBfStorage)(hashFunctionToUse)
  }

}
