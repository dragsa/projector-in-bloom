package org.gnat.cbf.filter

case class CBFMutable[T](override val positiveFalseProbability: Double,
                         override val expectedNumberOfElements: Int)(
                         implicit override val hashFunctionToUse: String)
  extends CBF[T](positiveFalseProbability, expectedNumberOfElements, hashFunctionToUse) {

  val bfStorage = new Array[Int](bfSize)

  def add(element: T): CBF[T] = {
//    val hashes = hashFunctionApplications(element)
//    println(hashes)
//    hashes.foreach(index => bfStorage(index) = bfStorage(index) + 1)
    hashFunctionApplications(element).foreach(index => bfStorage(index) = bfStorage(index) + 1)
    this
  }

  def addAll(elements: Set[T]): CBF[T] = {
//    elements.foreach(element => {val hashes = hashFunctionApplications(element); println(s"$hashes: $element"); hashes}.foreach(index => bfStorage(index) = bfStorage(index) + 1))
    elements.foreach(element => hashFunctionApplications(element).foreach(index => bfStorage(index) = bfStorage(index) + 1))
    this
  }

  def contains(element: T): Boolean = {
//    val hashes = hashFunctionApplications(element)
//
//    val test = hashes.map(bfStorage(_)).forall(_ > 0)
//    println(s"$hashes: $test - $element")
//    test
    hashFunctionApplications(element).map(bfStorage(_)).forall(_ > 0)
  }

  def containsAll(elements: Set[T]): Boolean =
    elements.forall(element => hashFunctionApplications(element).map(bfStorage(_)).forall(_ > 0))

  def remove(element: T): CBF[T] = {
    hashFunctionApplications(element).foreach(index => {
      val counter = bfStorage(index)
      if (counter == 0) ()
      else bfStorage(index) = counter - 1
    })
    this
  }

  def removeAll(elements: Set[T]): CBF[T] = {
    elements.foreach(element => hashFunctionApplications(element).foreach(index => {
      val counter = bfStorage(index)
      if (counter == 0) ()
      else bfStorage(index) = counter - 1
    }))
    this
  }

}
