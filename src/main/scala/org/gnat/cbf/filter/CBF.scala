package org.gnat.cbf.filter

import org.gnat.cbf.hash.HashFunctionTypes
import org.gnat.cbf.hash.HashProvider._

abstract class CBF[T](val positiveFalseProbability: Double, val expectedNumberOfElements: Int, val hashFunctionToUse: String) {

  val bfSize = Math.ceil(-expectedNumberOfElements * Math.log(positiveFalseProbability) / Math.pow(Math.log(2), 2)).toInt

  val bfHashFunctionsCount = Math.ceil(bfSize * Math.log(2) / expectedNumberOfElements).toInt

  protected def hashFunctionApplications: T => Seq[Int] = { hashedValue =>
    val hashes = Array.fill(bfHashFunctionsCount)(0).toSeq
    val hf = getHashFunction[T](hashFunctionToUse)(Integer.MAX_VALUE)(bfSize)
    // two independent hash functions combined in linear fashion
    val firstOrderHash = hf(hashedValue)
    val secondOrderMurmurHash = getHashFunction[T](HashFunctionTypes.Murmur3)(bfHashFunctionsCount)(bfSize)(hashedValue)
    //    println(s"F order: $firstOrderHash")
    //    println(s"S order: $secondOrderMurmurHash")
    val result = hashes.zipWithIndex.map { case (_, index) => (firstOrderHash + index * secondOrderMurmurHash) % bfSize }
    //    println(result)
    result
  }

  def add(element: T): CBF[T]

  def addAll(elements: Set[T]): CBF[T]

  def contains(element: T): Boolean

  def containsAll(elements: Set[T]): Boolean

  def remove(element: T): CBF[T]

  def removeAll(elements: Set[T]): CBF[T]
}