package org.gnat.cbf

import org.gnat.cbf.hash.{HashFunctionTypes, HashProvider}

import scala.util.Random

object HashTester extends App {
  val expectedNumberOfElements = 100

  val seedSame = Random.nextInt(Random.nextInt(Int.MaxValue))
  println("murmur with same seed applied:")
  val hfString1 = HashProvider.getHashFunction[String](HashFunctionTypes.Murmur3)
  println(s"${HashFunctionTypes.Murmur3} ${hfString1(seedSame)(expectedNumberOfElements)("testing murmur3")}")
  println()
  println(s"${HashFunctionTypes.Murmur3} ${hfString1(seedSame)(expectedNumberOfElements)("testing murmur3")}")
  println()
  println(s"${HashFunctionTypes.Murmur3} ${hfString1(Integer.MAX_VALUE)(expectedNumberOfElements)("M1rzGZN")}")
  println()
  println(s"${HashFunctionTypes.Murmur3} ${hfString1(Integer.MAX_VALUE)(expectedNumberOfElements)("zpr")}")
  println()

//  val tests = 8
  val tests = 700
  val collisions1 = new Array[Long](tests)
  val collisions2 = new Array[Long](tests)
  val collisions3 = new Array[Long](tests)

  // -1 +1 - to avoid zeros
  val seed = Random.nextInt(Random.nextInt(Int.MaxValue - 1) + 1)
//  def seed = Random.nextInt(Random.nextInt(Int.MaxValue - 1) + 1)
  println(s"for $tests random Strings:\n")
  for (i <- 0 until tests) {
    val testString = Random.alphanumeric.take((new Random).nextInt(10 - 1) + 1).mkString
    println(s"seed: $seed")
    println(s"value to hash: $testString")

    hashMetric(HashFunctionTypes.Murmur3, testString, seed, i, expectedNumberOfElements, collisions1)
    hashMetric(HashFunctionTypes.Byteswap, testString, seed, i, expectedNumberOfElements, collisions2)
    hashMetric(HashFunctionTypes.Cityhash, testString, seed, i, expectedNumberOfElements, collisions3)
    println()
  }
  println(s"Murmur collisions: ${tests - collisions1.distinct.length}")
  println(s"Byteswap collisions: ${tests - collisions2.distinct.length}")
  println(s"Cityhash collisions: ${tests - collisions3.distinct.length}")
  println("\n")

  def hashMetric(hfName: String, input: String, seed: Int, index: Int, expectedNumberOfElements: Int, collisionAcc: Array[Long]): Unit = {
    val hf = HashProvider.getHashFunction[String](hfName)
    val result = hf(seed)(expectedNumberOfElements)(input)
    println(s"$hfName: $result")
    collisionAcc(index) = result
  }

//  val seedList = Random.nextInt(Random.nextInt(Int.MaxValue))
////  def seedList = Random.nextInt(Random.nextInt(Int.MaxValue))
//  println(s"for $tests random List[Strings]:\n")
//  for (i <- 0 until tests) {
//    val testList = List.fill(3)(Random.alphanumeric.take((new Random).nextInt(5)).distinct.mkString)
//    println(s"seed: $seedStr")
//    println(s"value to hash: ${testList mkString " "}")
//    val hfList1 = HashProvider.getHashFunction[List[String]](HashFunction.Murmur3)
//    val result1 = hfList1(seedList)(expectedNumberOfElements)(testList)
//    println(s"${HashFunction.Murmur3} $result1")
//    collisions1(i) = result1
//    val hfList2 = HashProvider.getHashFunction[List[String]](HashFunction.Byteswap)
//    val result2 = hfList2(0)(expectedNumberOfElements)(testList)
//    println(s"${HashFunction.Byteswap} $result2")
//    println()
//    collisions2(i) = result2
//  }
//  println(s"Murmur collisions: ${tests - collisions1.distinct.length}")
//  println(s"Byteswap collisions: ${tests - collisions2.distinct.length}")
//  println()
}
