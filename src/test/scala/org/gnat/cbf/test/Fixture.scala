package org.gnat.cbf.test

import com.typesafe.config.ConfigFactory
import org.gnat.cbf.filter.CBF
import org.gnat.cbf.hash.HashFunctionTypes

trait Fixture {

  private val config = ConfigFactory.load("application-test.conf").getConfig("cbf-test")
  val expectedNumber = config.getInt("expected-number")
  val pfp = config.getDouble("positive-false-probability")

  implicit val hf: String = HashFunctionTypes.Murmur3

  def provideCBF: CBF[String]

  def fixtures = new {
    val testedInstance: CBF[String] = provideCBF
  }
}
