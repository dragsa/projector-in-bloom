package org.gnat.cbf.test.functional

import org.gnat.cbf.filter.{CBF, CBFMutable}

class CBFMutableSpec extends CBFBaseSpec {

  def provideCBF: CBF[String] = CBFMutable(pfp, expectedNumber)

}