package org.gnat.cbf.test.functional

import org.gnat.cbf.filter.CBF
import org.gnat.cbf.test.Fixture
import org.scalatest.{BeforeAndAfterAll, FlatSpec, Matchers}

import scala.util.Random

abstract class CBFBaseSpec extends FlatSpec with Matchers with Fixture with BeforeAndAfterAll {

  override def beforeAll() {
    println(s"hf count ${fixtures.testedInstance.bfHashFunctionsCount}")
    println(s"bf size ${fixtures.testedInstance.bfSize}")
  }

  //  "knowingly bad values with known parameters" should "result in CBF with collision" in {
  //    val testedInstanceAfterAction = fixtures.testedInstance.add("H7cbG40q")
  //      testedInstanceAfterAction.contains("iUFh") should be(true)
  //  }

  "add 'test' only" should "result in CBF with 'test' maybe present and 'some' absent" in {
    val testedInstanceAfterAction = fixtures.testedInstance.add("test")
    testedInstanceAfterAction.contains("test") should be(true)
    testedInstanceAfterAction.contains("some") should be(false)
  }

  "add 'test' and remove it in a row" should "result in CBF with 'test' absent" in {
    val testedInstanceAfterAction = fixtures.testedInstance.add("test_remove").remove("test_remove")
    testedInstanceAfterAction.contains("test_remove") should be(false)
  }

  "add several entries, but not 'some'" should "result in CBF with all entries maybe present and 'some' absent" in {
    val testedInstanceAfterAction = fixtures.testedInstance.addAll(Set("test", "boom", "dragon"))
    testedInstanceAfterAction.contains("test") should be(true)
    testedInstanceAfterAction.contains("boom") should be(true)
    testedInstanceAfterAction.contains("dragon") should be(true)
    testedInstanceAfterAction.contains("some") should be(false)
  }

  "add several entries and remove those in a row" should "result in CBF with all entries absent" in {
    val set = Set("test", "boom", "dragon")
    val testedInstanceAfterAction = fixtures.testedInstance.addAll(set).removeAll(set)
    testedInstanceAfterAction.contains("test") should be(false)
    testedInstanceAfterAction.contains("boom") should be(false)
    testedInstanceAfterAction.contains("dragon") should be(false)
  }

  "add several entries" should "result in CBF with all entries maybe present" in {
    val set = Set("test", "boom", "dragon")
    val testedInstanceAfterAction = fixtures.testedInstance.addAll(set)
    testedInstanceAfterAction.containsAll(set) should be(true)
  }

  "add random entries - significantly less than expected number of elements" should
    "result in lower FP rate than claimed" in {
    val testedInstance = fixtures.testedInstance
    val cutOff = testedInstance.expectedNumberOfElements
    val realPFP = positivesCounter(testedInstance, cutOff, "insert << expected")
    val expectedPFP = testedInstance.positiveFalseProbability
    realPFP should be(expectedPFP +- 10 * expectedPFP)
  }

  "add random entries - close to half of expected number of elements" should
    "result in higher FP rate than claimed" in {
    val testedInstance = fixtures.testedInstance
    val cutOff = testedInstance.expectedNumberOfElements * 3
    val realPFP = positivesCounter(testedInstance, cutOff, "insert < expected")
    val expectedPFP = testedInstance.positiveFalseProbability
    //    testedInstance.positiveFalseProbability should be >= realPFP
    realPFP should be(expectedPFP +- 10 * expectedPFP)
  }

  "add random entries - close to expected number of elements" should
    "result in higher FP rate than claimed" in {
    val testedInstance = fixtures.testedInstance
    val cutOff = testedInstance.expectedNumberOfElements * 5
    val realPFP = positivesCounter(testedInstance, cutOff, "insert ~= expected")
    val expectedPFP = testedInstance.positiveFalseProbability
    //    testedInstance.positiveFalseProbability should be >= realPFP
    realPFP should be(expectedPFP +- 10 * expectedPFP)
  }

  "add random entries - a little bit more than number of elements" should
    "result in higher FP rate than claimed" in {
    val testedInstance = fixtures.testedInstance
    val cutOff = testedInstance.expectedNumberOfElements * 10
    val realPFP = positivesCounter(testedInstance, cutOff, "insert > expected")
    testedInstance.positiveFalseProbability should be < realPFP
//    val expectedPFP = testedInstance.positiveFalseProbability
//    realPFP should be(expectedPFP +- 100 * expectedPFP)
  }

  "add random entries - significantly more than expected number of elements" should
    "result in significantly higher FP rate than claimed" in {
    val testedInstance = fixtures.testedInstance
    val cutOff = testedInstance.expectedNumberOfElements * 20
    val realPFP = positivesCounter(testedInstance, cutOff, "insert >> expected")
    testedInstance.positiveFalseProbability should be < realPFP
  }

  def positivesCounter(testedInstance: CBF[String], cutOff: Int, scenarioName: String): Double = {
    val (elementsPresent, elementsAbsent) = {
      Seq.fill(cutOff)(Random.alphanumeric.take((new Random).nextInt(10)).mkString).toSet.splitAt(cutOff / 5)
    }
    println(s"\n$scenarioName:\ngenerated number of elements: $cutOff\nnumber to be inserted: ${elementsPresent.size}\nnumber to be absent: ${elementsAbsent.size}")
    val fullBF = testedInstance.addAll(elementsPresent)
    //    val truePositive = elementsPresent.foldLeft(0)((acc, next) => {println(s"$next\nGOOD"); if (fullBF.contains(next)) acc + 1 else acc})
    //    val falsePositive = elementsAbsent.foldLeft(0)((acc, next) => {println(s"$next\nBAD"); if (fullBF.contains(next)) {println("BOOM!"); acc + 1} else acc})
    val truePositive = elementsPresent.foldLeft(0)((acc, next) => if (fullBF.contains(next)) acc + 1 else acc)
    val falsePositive = elementsAbsent.foldLeft(0)((acc, next) => if (fullBF.contains(next)) acc + 1 else acc)
    val realPFP = falsePositive.toDouble / (elementsAbsent.size + elementsPresent.size).toDouble
    println(s"\ntrue positive: $truePositive\nfalse positive: $falsePositive\nexpected PFP: ${testedInstance.positiveFalseProbability}\nreal PFP: $realPFP")
    realPFP
  }

}
