package org.gnat.cbf.test.functional

import org.gnat.cbf.filter.{CBF, CBFImmutable}

class CBFImmutableSpec extends CBFBaseSpec {

  def provideCBF: CBF[String] = CBFImmutable(pfp, expectedNumber)

}