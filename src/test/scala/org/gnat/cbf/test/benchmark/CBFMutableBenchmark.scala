package org.gnat.cbf.test.benchmark

import org.gnat.cbf.filter.{CBF, CBFMutable}

object CBFMutableBenchmark extends CBFBaseBenchmark {

  lazy val testedInstanceName: String = s"BCF Mutable for $hf"

  def provideCBF: CBF[String] = CBFMutable(pfp, expectedNumber)

}
