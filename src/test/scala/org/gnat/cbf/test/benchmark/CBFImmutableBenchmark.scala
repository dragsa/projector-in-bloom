package org.gnat.cbf.test.benchmark

import org.gnat.cbf.filter.{CBF, CBFImmutable}

object CBFImmutableBenchmark extends CBFBaseBenchmark {

  lazy val testedInstanceName: String = s"BCF Immutable for $hf"

  def provideCBF: CBF[String] = CBFImmutable(pfp, expectedNumber)

}
