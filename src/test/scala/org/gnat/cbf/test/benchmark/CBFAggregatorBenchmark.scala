//package org.gnat.cbf.test.benchmark
//
//import org.scalameter.Bench
//import org.scalameter.api._
//
//object CBFAggregatorBenchmark extends Bench.Group {
//
//  performance of "ICBF" config (
//    reports.resultDir -> "target/benchmarks/icbf"
//    ) in {
//    include(new CBFImmutableBenchmark {})
//  }
//
//  performance of "MCBF" config (
//    reports.resultDir -> "target/benchmarks/mcbf"
//    ) in {
//    include(new CBFMutableBenchmark {})
//  }
//}