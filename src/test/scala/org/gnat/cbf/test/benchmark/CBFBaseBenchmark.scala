package org.gnat.cbf.test.benchmark

import org.gnat.cbf.test.Fixture
import org.scalameter.api._
import scala.util.Random

abstract class CBFBaseBenchmark extends Bench.OfflineReport with Fixture {

  import org.scalameter.picklers.Implicits._

  def testedInstanceName: String

  val singleTestData: Gen[String] = Gen.single("value")("test")
  val multipleTestDataArraySizes: Gen[Int] = Gen.range("size")(1000, 10000, 1000)
  val multipleTestData: Gen[Set[String]] =
    for (size <- multipleTestDataArraySizes)
      yield Seq.fill(size)(Random.alphanumeric.take((new Random).nextInt(10)).mkString).toSet

  performance of testedInstanceName config (
    exec.benchRuns -> 100
//    ,reports.resultDir -> s"target/benchmarks/$testedInstanceName"
    ) in {

    measure method "add" in {
      using(singleTestData) in {
        r => {
          fixtures.testedInstance.add(r)
        }
      }
    }

    measure method "add + search negative" in {
      using(singleTestData) in {
        r => {
          fixtures.testedInstance.add(r).contains("something_wicked_this_way_comes")
        }
      }
    }

    measure method "addAll" in {
      using(multipleTestData) in {
        r => {
          fixtures.testedInstance.addAll(r)
        }
      }
    }

    measure method "addAll + removeAll" in {
      using(multipleTestData) in {
        r => {
          fixtures.testedInstance.addAll(r).removeAll(r)
        }
      }
    }

    measure method "containsAll" in {
      using(multipleTestData) in {
        _ => {
          fixtures.testedInstance.containsAll(Set("boom", "dragon", "something"))
        }
      }
    }
  }

}
